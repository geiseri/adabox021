# AdaBox021 Project

* Espressif ESP32-S3 with 3.3v logic/power. It has 4MB of Flash and 2MB of PSRA
* OV5640 camera module. It has a 72 degree view, 5MP sensor and an auto-focus motor.

    VSYNC - GPIO5. VSYNC_GPIO_NUM.
    HREF - GPIO6.  HREF_GPIO_NUM.
    XCLK - GPIO8. XCLK_GPIO_NUM.
    PCLK - GPIO11.  PCLK_GPIO_NUM.
    PWDN - GPIO21.  PWDN_GPIO_NUM.
    DATA2 - GPIO13.  Y2_GPIO_NUM.
    DATA3 - GPIO15.  Y3_GPIO_NUM.
    DATA4 - GPIO16.  Y4_GPIO_NUM.
    DATA5 - GPIO14.  Y5_GPIO_NUM.
    DATA6 - GPIO12.  Y6_GPIO_NUM.
    DATA7 - GPIO10.  Y7_GPIO_NUM.
    DATA8 - GPIO9.  Y8_GPIO_NUM.
    DATA9 - GPIO7. Y9_GPIO_NUM.
    RESET - GPIO47. RESET_GPIO_NUM

* ST7789 1.54" 240x240 Color TFT. It uses the following pins for SPI communication:

    MOSI - GPIO35.   MOSI.
    SCK - GPIO36.   SCK.
    MISO - GPIO37. MISO.
    DC - GPIO40.  TFT_DC.
    CS - GPIO39. TFT_CS.
    Backlight - GPIO45.  TFT_BACKLIGHT.
    Reset - GPIO38. TFT_RESET or TFT_RST.

* AW9523 GPIO Expander. The IO Expander is connected via the I2C bus at address  0x58.

LIS3DH accelerometer. I2C on address 0x19. Its interrupt (IRQ) pin is connected to GPIO3.

Front buttons are connected through the AW9523 GPIO expander:

    OK button - pin 11
    Select button - pin 1
    Up button - pin 13
    Down button - pin 15
    Left button - pin 14
    Right button - pin 12

* Analog MEMS microphone input from the microphone is connected to GPIO2.

* Speaker is connected to GPIO46. It is muted via pin 0 on the AW9523 GPIO expander.

* Two 3-pin JST connectors with digital/analog IO. A0 is connected to GPIO17 and A1 is connected to GPIO18. Between both connectors is a jumper labeled 5V3. It can be cut and soldered to use 3V instead of 5V for the VCC signal on the JST connectors.

* One 4-pin Stemma QT connector. The I2C has pullups to 3.3V power. The I2C pins are connected to GPIO34 (SDA) and GPIO33 (SCL).

* MicroSD card slot.
    SDCS (chip select pin)  - connected to GPIO48, SD_CS or SD_CHIP_SELECT.
    SDCD (card detect pin) - connected to pin 9 on the AW9523.

* NeoPixel is connected to GPIO1, PIN_NEOPIXEL or NEOPIXEL_PIN.

* USB-C port - This is used for both powering and programming the board. You can power it with any USB C cable. When USB is plugged in it will charge the LiPoly battery.
    Battery Monitor Pin - You can monitor the battery percentage via an ADC pin BATT_MONITOR in Arduino.
