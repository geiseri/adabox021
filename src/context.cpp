#include "context.h"
#include <numeric>

Context* Context::ctx = nullptr;

Context* Context::instance() {
  if (ctx == nullptr) {
    ctx = new Context();
  }
  return ctx;
}

int decode_jpeg_callback(JPEGDRAW* pDraw) {
  Context::instance()->localFb->drawRGBBitmap(
    pDraw->x,
    pDraw->y,
    pDraw->pPixels,
    pDraw->iWidth,
    pDraw->iHeight
  );
  return 1; // returning true (1) tells JPEGDEC to continue decoding. Returning false (0) would quit decoding immediately.
}

Context::Context() {
  localFb = new MyFB<240, 240>();
  jpeg_decoder.setPixelType(RGB565_BIG_ENDIAN);
  heat_update_stamp = millis();

}

/*
https://www.nxp.com/docs/en/application-note/AN3461.pdf

Range -9.8 - 9.8
From Back of Camera

Roll is forwards and backwards
Pitch is side to side
*/
std::pair<int, int> Context::get_accel_points(int roll_len, int pitch_len) {
  float x_ms2, y_ms2, z_ms2 = 0;
  if (pycamera.readAccelData(&x_ms2, &y_ms2, &z_ms2)) {



    auto sign = z_ms2 > 0 ? 1 : -1;
    auto miu = 0.01;
    // θ
    float pitch_angle = atan2(-x_ms2, sqrt((y_ms2 * y_ms2) + (z_ms2 * z_ms2))) * 180.f / M_PI;
    // φ
    float roll_angle = (atan2(y_ms2, sign * sqrt((z_ms2 * z_ms2) + (miu * x_ms2 * x_ms2))) * 180.f / M_PI) + 90;

    int pitch = map_value<float, int>(pitch_angle, -90, 90, 0, pitch_len);
    int roll = map_value<float, int>(roll_angle, -90, 90, 0, roll_len);

    //localFb->setCursor(20, 20);
    //localFb->setTextSize(1);
    //localFb->printf("X: %04.2f, Y: %04.2f, Z: %04.2f", x_ms2, y_ms2, z_ms2);
    //localFb->setCursor(20, 30);
    //localFb->printf("R: %04.2f / %d, P: %04.2f / %d", roll_angle, roll, pitch_angle, pitch);
    return std::make_pair(roll, pitch);
  }
  return std::make_pair(50, 50);
}

void Context::cameraTone(uint8_t pin, unsigned int frequency, unsigned long duration) {
  (void) pin;
  pycamera.speaker_tone(frequency, duration);
}

uint16_t Context::rgb565ColorWheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return pycamera.color565(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return pycamera.color565(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pycamera.color565(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void Context::setup_tof_camera() {
  tof_.setup();
}

void Context::update_tof_camera() {
  tof_.update();
  tof_.draw<0, 0, 64, 64>(localFb);
  localFb->setCursor(0, 64 + 10);
  localFb->setTextSize(1);
  localFb->setTextColor(RGB565<255, 0, 0>());
  localFb->print(tof_.display_string.c_str());
}

void Context::update_microphone() {
  mic->update_fb(localFb);
  localFb->setCursor(0, mic->fft_frame.y - 20);
  localFb->setTextSize(1);
  localFb->setTextColor(RGB565<255, 0, 0>());
  localFb->printf("peak: %04.2f% Hz @ %04.2f,\nSample range: %04.2f,%04.2f", mic->peak_frequeny.output, mic->peak_mag.output, mic->sample_min, mic->sample_max);
}

void Context::resolution_up() {
  set_resolution(resolution_ + 1);
}
void Context::resolution_down() {
  set_resolution(resolution_ - 1);
}
bool Context::set_resolution(size_t new_res) {
  resolution_ = std::clamp(new_res, 0u, sizeof(validSizes) - 1);
  camera_frame.set_clamp_sizes(validSizes[resolution_]);
  return pycamera.setFramesize(validSizes[resolution_]);
}

bool Context::setup_camera() {
  if (pycamera.begin()) {
    Serial.println("Camera up");
    delay(500);
    camera_frame.set_clamp_sizes(validSizes[resolution_]);
    return pycamera.setFramesize(validSizes[resolution_]);
  }
  pycamera.ring.setBrightness(0xff);
  pycamera.setRing(0xFFFFFF);
  return false;
}

bool Context::setup_thermal_camera() {
#ifdef ENABLE_THERMAL_CAMERA
  return mlx_.begin();
#else
  return true;
#endif
}

void Context::test_screen() {
  static uint8_t screenloops = 0;
  localFb->fillScreen(rgb565ColorWheel(screenloops));
  screenloops += 4;
  if (screenloops == 0xFF) {
    screenloops = 0;
  }
}


void Context::do_camera_grab() {
  Profiler::instance()->camera.start_stamp();
  pycamera.captureFrameHook([this](camera_fb_t* camera) {
    Profiler::instance()->jpeg_decode.start_stamp();
    jpeg_decoder.openRAM(camera->buf, camera->len, &decode_jpeg_callback);
    localFb->fillRect(
      camera_frame.x,
      camera_frame.y,
      camera_frame.w,
      camera_frame.h,
      RGB565<25, 25, 25>()
    );
    bool ok = false;
    if (camera->format == PIXFORMAT_JPEG) {
      ok = jpeg_decoder.decode(
        camera_frame.x + camera_frame.xoff,
        camera_frame.y + camera_frame.yoff,
        camera_frame.scale) == 1;
    }
    Profiler::instance()->jpeg_decode.end_stamp();
    return ok;
    });
  Profiler::instance()->camera.end_stamp();

}


void Context::do_thermalcam_drawing() {

#ifdef ENABLE_THERMAL_CAMERA
  Profiler::instance()->thermal.start_stamp();
  mlx_.capture();
  static constexpr uint16_t box_height = 64;
  static constexpr uint16_t box_width = box_height * 1.3; // 1.3x height
  static constexpr uint16_t box_x_offset = localFb->fb_width - box_width;
  static constexpr uint16_t box_y_offset = localFb->fb_height - box_height;
  localFb->drawRect(box_x_offset, box_y_offset, box_width, box_height, RGB565<255, 255, 255>());
  mlx_.draw<box_x_offset, box_y_offset, box_width, box_height>(localFb);
  Profiler::instance()->thermal.end_stamp();
#endif  
}

void Context::handle_buttons() {
  pycamera.readButtons();
  if (pycamera.justPressed(AWEXP_SD_DET)) {
    Serial.println(F("SD Card removed"));
    pycamera.endSD();
    localFb->setCursor(0, 32);
    localFb->setTextSize(2);
    localFb->setTextColor(RGB565<255, 0, 0>());
    localFb->print(F("SD Card removed"));
    delay(200);
  }
  if (pycamera.justReleased(AWEXP_SD_DET)) {
    Serial.println(F("SD Card inserted!"));
    pycamera.initSD();
    localFb->setCursor(0, 32);
    localFb->setTextSize(2);
    localFb->setTextColor(RGB565<255, 0, 0>());
    localFb->print(F("SD Card inserted"));
    delay(200);
  }
  if (pycamera.justPressed(AWEXP_BUTTON_RIGHT)) {
#ifdef ENABLE_THERMAL_CAMERA
    mlx_.range_next();
#else
    pycamera.specialEffect = (pycamera.specialEffect + 1) % 7;
    pycamera.setSpecialEffect(pycamera.specialEffect);
    delay(200);
#endif
  }
  if (pycamera.justPressed(AWEXP_BUTTON_LEFT)) {
#ifdef ENABLE_THERMAL_CAMERA
    mlx_.range_prev();
#else
    pycamera.specialEffect = (pycamera.specialEffect + 6) % 7;
    pycamera.setSpecialEffect(pycamera.specialEffect);
    delay(200);
#endif
  }
  if (pycamera.justPressed(AWEXP_BUTTON_UP)) {
    resolution_up();
    delay(200);
  }
  if (pycamera.justPressed(AWEXP_BUTTON_DOWN)) {
    resolution_down();
    delay(200);
  }
  if (pycamera.justReleased(SHUTTER_BUTTON)) {
    pycamera.speaker_tone(100, 500);
    if (!save_thermal_camera()) {
      Serial.println("Thermalamera failed");
    }
    if (!save_camera()) {
      Serial.println("Camera failed");
    }

    display_message = "Snap!";
    message_timeout = 500;
    delay(200);
  }
  if (pycamera.justReleased(AW_SEL_MASK)) {
    display_message = "Select!";
    message_timeout = 500;
    delay(200);
  }
}

void Context::update_battery_stats() {
  //float A0_voltage = analogRead(A0) / 4096.0 * 3.3;
  //float A1_voltage = analogRead(A1) / 4096.0 * 3.3;
  localFb->setCursor(0, 190);
  localFb->setTextSize(1);
  localFb->setTextColor(RGB565<255, 255, 255>());
  //localFb->print("A0 = ");
  //localFb->print(A0_voltage, 1);
  //localFb->print("V, A1 = ");
  //localFb->print(A1_voltage, 1);
  localFb->printf("Bat: %04.2fV ", pycamera.readBatteryVoltage());
}

void Context::update_camera_stats() {
  // print the camera frame size
  bool show_debug = false;

  if (display_message.size() > 0) {
    localFb->setCursor(25, 100);
    localFb->setTextSize(1);
    localFb->setTextColor(RGB565<255, 255, 255>());
    int16_t tx1 = 0;
    int16_t ty1 = 0;
    uint16_t tw = 0;
    uint16_t th = 0;
    localFb->getTextBounds(display_message.c_str(), 25, 100, &tx1, &ty1, &tw, &th);
    localFb->fillRect(tx1, ty1, tw, th, 0x0000);
    localFb->println(display_message.c_str());
    message_timeout--;
    if (message_timeout < 0) {
      display_message.clear();
    }
  }
  localFb->setCursor(200, 10);
  localFb->setTextSize(1);
  localFb->setTextColor(RGB565<255, 255, 255>());
  localFb->print("Size:");
  switch (pycamera.camera->status.framesize) {
  case FRAMESIZE_96X96:
    localFb->print("96x96");
    break;
  case FRAMESIZE_QQVGA:
    localFb->print("160x120");
    break;
  case FRAMESIZE_QCIF:
    localFb->print("176x144");
    break;
  case FRAMESIZE_HQVGA:
    localFb->print("240x176");
    break;
  case FRAMESIZE_240X240:
    localFb->print("240x240");
    break;
  case FRAMESIZE_QVGA:
    localFb->print("320x240");
    break;
  case FRAMESIZE_CIF:
    localFb->print("400x296");
    break;
  case FRAMESIZE_HVGA:
    localFb->print("480x320");
    break;
  case FRAMESIZE_VGA:
    localFb->print("640x480");
    break;
  case FRAMESIZE_SVGA:
    localFb->print("800x600");
    break;
  case FRAMESIZE_XGA:
    localFb->print("1024x768");
    break;
  case FRAMESIZE_HD:
    localFb->print("1280x720");
    break;
  case FRAMESIZE_SXGA:
    localFb->print("1280x1024");
    break;
  case FRAMESIZE_UXGA:
    localFb->print("1600x1200");
    break;
  default:
    localFb->print("Unknown");
    break;
  }
  localFb->setCursor(200, 20);
  localFb->setTextSize(1);
  if (show_debug) {
    multi_heap_info_t info;
    heap_caps_get_info(&info, MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT); // internal RAM, memory capable to store data or to create new task
    info.total_free_bytes;   // total currently free in all non-continues blocks
    info.minimum_free_bytes;  // minimum free ever
    info.largest_free_block;   // largest continues block to allocate big array
    localFb->printf("Free: %d/%d", info.total_free_bytes, info.largest_free_block);
  }
#ifdef ENABLE_THERMAL_CAMERA
  else {
    localFb->print(mlx_.display_string.c_str());
  }
#endif
  /*
    localFb->setCursor(0, 220);
    localFb->setTextSize(1);
    localFb->printf("L: %dms, C: %dms D: %dms", Profiler::instance()->full_loop.delta, Profiler::instance()->camera.delta, Profiler::instance()->blit.delta);
    localFb->setCursor(0, 230);
    localFb->printf("C: %dms, O: %dms T: %dms", Profiler::instance()->camera.delta, Profiler::instance()->outside_loop.delta, Profiler::instance()->thermal.delta);
  #ifdef ENABLE_THERMAL_CAMERA
    localFb->printf("G: %dms, C: %dms T: %dms", Context::instance()->mlx_.grab_delta, Context::instance()->mlx_.calc_delta, Profiler::instance()->thermal.delta);
  #else
    //localFb->printf("Mic: %d", Context::instance()->mic_value);
  #endif
  */
}

void Context::update_accelerometer_stats() {
  Profiler::instance()->acceleromiter.start_stamp();
  auto [roll, pitch] = get_accel_points(localFb->fb_width, localFb->fb_height);

  /*
      x,h-6
      *
     * *
    *   *
   *  X  *
  x-3,h   x+3,h
  */
  localFb->fillTriangle(
    pitch - 6, localFb->fb_height,
    pitch + 6, localFb->fb_height,
    pitch, localFb->fb_height - 12,
    RGB565<255, 0, 0>());

  /*
  y-3,w
  *
  * *
  *  *y,y-6
  * *
  *
  y+3,w
  */
  localFb->fillTriangle(
    localFb->fb_width - 6, roll - 6,
    localFb->fb_width - 6, roll + 6,
    localFb->fb_width - 12, roll,
    RGB565<255, 0, 0>());
  Profiler::instance()->acceleromiter.end_stamp();
}

void Context::send_to_screen() {
  Profiler::instance()->blit.start_stamp();
  pycamera.drawRGBBitmap(0, 0, localFb->getBuffer(), localFb->fb_width, localFb->fb_height);
  localFb->fillScreen(RGB565<0, 0, 0>());
  Profiler::instance()->blit.end_stamp();
}


void Context::loop_neopixel() {
  static uint8_t loopn = 0;
  pycamera.setNeopixel(pycamera.Wheel(loopn));

  if (loopn == 254) {
    loopn = 0;
  }
  loopn += 8;
}

bool Context::save_thermal_camera() {
#ifdef ENABLE_THERMAL_CAMERA
  bool ok = false;

  char fullfilename[64];
  for (int inc = 0; inc <= 1000; inc++) {
    if (inc == 1000)
      return false;
    snprintf(fullfilename, sizeof(fullfilename), "%s%03d.csv", "heat-",
      inc);
    if (!pycamera.sd.exists(fullfilename))
      break;
  }
  auto file = pycamera.sd.open(fullfilename, O_WRITE | O_CREAT);
  if (file.isOpen()) {
    auto bits = mlx_.get_report();
    auto wrote = file.write(bits.data(), bits.size());
    Serial.printf("Wrote %d of %d bits to %s\n", wrote, bits.size(), fullfilename);
    file.close();
    ok = wrote == bits.size();
  }
  else {
    Serial.printf("Save to %s failed\n", fullfilename);
  }

  return ok;
#else 
  return true;
#endif
}


bool Context::save_camera() {
  bool ok = false;
  char fullfilename[64];
  for (int inc = 0; inc <= 1000; inc++) {
    if (inc == 1000)
      return false;
    snprintf(fullfilename, sizeof(fullfilename), "%s%03d.jpg", "image-", inc);
    if (!pycamera.sd.exists(fullfilename))
      break;
  }
  //pycamera.ring.setBrightness(0xff);
  //pycamera.setRing(0xFFFFFF);
  delay(100);
  // up the resolution to FRAMESIZE_HD
  // grab frame 2x
  pycamera.setFramesize(FRAMESIZE_HD);
  auto frame = esp_camera_fb_get();
  if (!frame) {
    esp_camera_fb_return(frame);
    set_resolution(resolution_); // Change it back
    Serial.println("Couldnt capture first frame");
    return false;
  }
  esp_camera_fb_return(frame);
  esp_camera_fb_return(esp_camera_fb_get()); // Next good one

  Serial.println("Get it now!");
  frame = esp_camera_fb_get();  // get it for real
  if (!frame) {
    esp_camera_fb_return(frame);
    set_resolution(resolution_); // Change it back
    Serial.println("Couldnt capture second frame");
    return false;
  }
  //pycamera.ring.setBrightness(0xf);
  auto file = pycamera.sd.open(fullfilename, O_WRITE | O_CREAT);

  if (file.isOpen()) {
    auto wrote = file.write(frame->buf, frame->len);
    Serial.printf("Wrote %d of %d bits to %s\n", wrote, frame->len, fullfilename);
    file.close();
    ok = wrote == frame->len;
    file.flush();
  }
  else {
    Serial.printf("Save to %s failed\n", fullfilename);
  }
  esp_camera_fb_return(frame);
  set_resolution(resolution_);
  frame = esp_camera_fb_get(); // Clear it so we dont feak out when we go back
  delay(100);
  esp_camera_fb_return(frame);

  return ok;
}
