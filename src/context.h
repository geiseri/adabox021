#pragma once
#include <Ewma.h>
#include <Adafruit_PyCamera.h>
#include <JPEGDEC.h>
#include <cstdint>
#include <memory>

#ifdef ENABLE_THERMAL_CAMERA
#include "thermalcamera.h"
#endif

#include "image.h"
#include "helpers.h"
#include "rtos.h"
#include "microphone.h"
#include "tofcamera.h"



struct Context {
  Adafruit_PyCamera pycamera;
  size_t resolution_ = 3; // FRAMESIZE_240X240
#ifdef ENABLE_THERMAL_CAMERA
  ThermalCamera<24, 32> mlx_;
#endif
TofCamera tof_;
  rect_t<0, 0, 240, 200> camera_frame;
  uint64_t heat_update_stamp = 0;
  MyFB<240, 240>* localFb = nullptr;
  JPEGDEC jpeg_decoder;
  std::string display_message;
  int message_timeout = 0;
  Microphone* mic = nullptr;
  static Context* instance();
  void update_thermals(int x_pos, int y_pos, int box_width, int box_height);
  std::pair<int, int> get_accel_points(int roll_len, int pitch_len);
  void cameraTone(uint8_t pin, unsigned int frequency, unsigned long duration);
  void do_camera_grab();
  void do_thermalcam_drawing();
  void handle_buttons();
  void update_battery_stats();
  void update_camera_stats();
  void update_accelerometer_stats();
  void update_tof_camera();
  void send_to_screen();
  bool setup_camera();
  void loop_neopixel();
  void test_screen();
  uint16_t rgb565ColorWheel(byte pos);
  void setup_temp_sensor();
  void setup_tof_camera();
  void update_microphone();
  void resolution_up();
  void resolution_down();
  bool set_resolution(size_t new_res);
  bool save_thermal_camera();
  bool setup_thermal_camera();
  bool save_camera();

private:
  Context();
  static Context* ctx;
};

