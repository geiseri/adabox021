#pragma once

#include <esp_camera.h>
#include <JPEGDEC.h>
#include <Adafruit_GFX.h> 

#include "rtos.h"
#include "rescale_const.h"
namespace detail {
  constexpr int scale_for_range(int scale) {
    if (scale > 4) return JPEG_SCALE_EIGHTH;
    if (scale > 2) return JPEG_SCALE_QUARTER;
    if (scale > 1) return JPEG_SCALE_HALF;
    return 1;
  }


  // Camera size, destination frame size
  template<int16_t CW, int16_t CH, int16_t FW, int16_t FH>
  std::tuple<int, int16_t, int16_t> parts() {
    constexpr auto scale = detail::scale_for_range(std::ceil((float) CW / (float) FW));
    constexpr auto xoff = std::clamp((FW - ((float) CW / scale)) / 2.f, 0.f, (float) FW);
    constexpr auto yoff = std::clamp((FH - ((float) CH / scale)) / 2.f, 0.f, (float) FH);
    return std::make_tuple(
      scale,
      xoff,
      0
    );
  }
}

struct profiler_tracker {
  int last_stamp = 0;
  int delta = 0;
  int calls = 0;
  int time = 0;
  void end_stamp() {
    delta = millis() - last_stamp;
    time += delta;
    last_stamp = 0;
  }
  void start_stamp() {
    last_stamp = millis();
    calls++;
  }
};

struct Profiler {
  profiler_tracker camera{};
  profiler_tracker thermal{};
  profiler_tracker jpeg_decode{};
  profiler_tracker acceleromiter{};
  profiler_tracker blit{};
  profiler_tracker full_loop{};
  profiler_tracker outside_loop{};

  static Profiler* instance() {
    if (p_ == nullptr) {
      p_ = new Profiler();
    }
    return p_;
  }

private:
  Profiler() {};
  static Profiler* p_;
};

using mic_stream_buffer = typename FreeRTOS::stream_buffer<64, float>;

template<int16_t X, int16_t Y, int16_t W, int16_t H>
struct rect_t {
  static constexpr int16_t x = X;
  static constexpr int16_t y = Y;
  static constexpr int16_t w = W;
  static constexpr int16_t h = H;
  int16_t xoff = 0;
  int16_t yoff = 0;
  int scale = 1;
  void set_clamp_sizes(framesize_t size) {
    if (size == FRAMESIZE_96X96) std::tie(scale, xoff, yoff) = detail::parts<96, 96, W, H>();
    if (size == FRAMESIZE_QQVGA) std::tie(scale, xoff, yoff) = detail::parts<160, 120, W, H>();
    if (size == FRAMESIZE_QCIF) std::tie(scale, xoff, yoff) = detail::parts<176, 144, W, H>();
    if (size == FRAMESIZE_HQVGA) std::tie(scale, xoff, yoff) = detail::parts<240, 176, W, H>();
    if (size == FRAMESIZE_240X240)  std::tie(scale, xoff, yoff) = detail::parts<240, 240, W, H>();
    if (size == FRAMESIZE_QVGA) std::tie(scale, xoff, yoff) = detail::parts<320, 240, W, H>();
    if (size == FRAMESIZE_CIF) std::tie(scale, xoff, yoff) = detail::parts<400, 296, W, H>();
    if (size == FRAMESIZE_HVGA) std::tie(scale, xoff, yoff) = detail::parts<480, 320, W, H>();
    if (size == FRAMESIZE_VGA)std::tie(scale, xoff, yoff) = detail::parts<640, 480, W, H>();
    if (size == FRAMESIZE_SVGA)std::tie(scale, xoff, yoff) = detail::parts<800, 600, W, H>();
    if (size == FRAMESIZE_XGA)std::tie(scale, xoff, yoff) = detail::parts<1024, 768, W, H>();
    if (size == FRAMESIZE_HD)std::tie(scale, xoff, yoff) = detail::parts<1280, 720, W, H>();
    if (size == FRAMESIZE_SXGA)std::tie(scale, xoff, yoff) = detail::parts<1280, 1024, W, H>();
    if (size == FRAMESIZE_UXGA)std::tie(scale, xoff, yoff) = detail::parts<1600, 1200, W, H>();
    //Serial.printf("Set frame: s: %d, xoff: %d, yoff: %d, for x: %d y: %d w: %d h: %d\n",scale,xoff,yoff,x,y,w,h);
  }
};

template<uint8_t R, uint8_t G, uint8_t B>
constexpr uint16_t RGB565() {
  return ((R & 0xF8) << 8) | ((G & 0xFC) << 3) | (B >> 3);
}


template <typename In, typename Out = In>
Out map_value(In x, In in_min, In in_max, Out out_min, Out out_max) {
  const In run = in_max - in_min;
  if (run == 0) {
    log_e("map_value(): Invalid input range, min == max");
    return 0;
  }
  const Out rise = out_max - out_min;
  const Out delta = x - in_min;
  return static_cast<Out>(std::clamp(static_cast<Out>(delta * rise / run + out_min), out_min, out_max));
}

constexpr static framesize_t validSizes[] = {
    FRAMESIZE_96X96,    // 96x96
    FRAMESIZE_QQVGA,    // 160x120
    FRAMESIZE_QCIF,     // 176x144
    FRAMESIZE_HQVGA,    // 240x176
    FRAMESIZE_240X240,  // 240x240 900k
    FRAMESIZE_QVGA,     // 320x240
    FRAMESIZE_CIF,      // 400x296
    FRAMESIZE_HVGA,     // 480x320
    FRAMESIZE_VGA,      // 640x480 4,800k
    FRAMESIZE_SVGA,     // 800x600
    FRAMESIZE_XGA,      // 1024x768
    FRAMESIZE_HD,       // 1280x720
    FRAMESIZE_SXGA,     // 1280x1024
    FRAMESIZE_UXGA,     // 1600x1200
};


constexpr static uint16_t heat_pallet[] = {
  0x480F,0x400F,0x400F,0x400F,0x4010,0x3810,0x3810,0x3810,0x3810,0x3010,0x3010,0x3010,0x2810,0x2810,0x2810,0x2810,
  0x2010,0x2010,0x2010,0x1810,0x1810,0x1811,0x1811,0x1011,0x1011,0x1011,0x0811,0x0811,0x0811,0x0011,0x0011,0x0011,
  0x0011,0x0011,0x0031,0x0031,0x0051,0x0072,0x0072,0x0092,0x00B2,0x00B2,0x00D2,0x00F2,0x00F2,0x0112,0x0132,0x0152,
  0x0152,0x0172,0x0192,0x0192,0x01B2,0x01D2,0x01F3,0x01F3,0x0213,0x0233,0x0253,0x0253,0x0273,0x0293,0x02B3,0x02D3,
  0x02D3,0x02F3,0x0313,0x0333,0x0333,0x0353,0x0373,0x0394,0x03B4,0x03D4,0x03D4,0x03F4,0x0414,0x0434,0x0454,0x0474,
  0x0474,0x0494,0x04B4,0x04D4,0x04F4,0x0514,0x0534,0x0534,0x0554,0x0554,0x0574,0x0574,0x0573,0x0573,0x0573,0x0572,
  0x0572,0x0572,0x0571,0x0591,0x0591,0x0590,0x0590,0x058F,0x058F,0x058F,0x058E,0x05AE,0x05AE,0x05AD,0x05AD,0x05AD,
  0x05AC,0x05AC,0x05AB,0x05CB,0x05CB,0x05CA,0x05CA,0x05CA,0x05C9,0x05C9,0x05C8,0x05E8,0x05E8,0x05E7,0x05E7,0x05E6,
  0x05E6,0x05E6,0x05E5,0x05E5,0x0604,0x0604,0x0604,0x0603,0x0603,0x0602,0x0602,0x0601,0x0621,0x0621,0x0620,0x0620,
  0x0620,0x0620,0x0E20,0x0E20,0x0E40,0x1640,0x1640,0x1E40,0x1E40,0x2640,0x2640,0x2E40,0x2E60,0x3660,0x3660,0x3E60,
  0x3E60,0x3E60,0x4660,0x4660,0x4E60,0x4E80,0x5680,0x5680,0x5E80,0x5E80,0x6680,0x6680,0x6E80,0x6EA0,0x76A0,0x76A0,
  0x7EA0,0x7EA0,0x86A0,0x86A0,0x8EA0,0x8EC0,0x96C0,0x96C0,0x9EC0,0x9EC0,0xA6C0,0xAEC0,0xAEC0,0xB6E0,0xB6E0,0xBEE0,
  0xBEE0,0xC6E0,0xC6E0,0xCEE0,0xCEE0,0xD6E0,0xD700,0xDF00,0xDEE0,0xDEC0,0xDEA0,0xDE80,0xDE80,0xE660,0xE640,0xE620,
  0xE600,0xE5E0,0xE5C0,0xE5A0,0xE580,0xE560,0xE540,0xE520,0xE500,0xE4E0,0xE4C0,0xE4A0,0xE480,0xE460,0xEC40,0xEC20,
  0xEC00,0xEBE0,0xEBC0,0xEBA0,0xEB80,0xEB60,0xEB40,0xEB20,0xEB00,0xEAE0,0xEAC0,0xEAA0,0xEA80,0xEA60,0xEA40,0xF220,
  0xF200,0xF1E0,0xF1C0,0xF1A0,0xF180,0xF160,0xF140,0xF100,0xF0E0,0xF0C0,0xF0A0,0xF080,0xF060,0xF040,0xF020,0xF800,
};


struct color_range {
  uint8_t min;
  uint8_t max;
};

template <typename In>
uint16_t heat_color(In value, In min, In max, color_range range = { 0,255 }) {

  return heat_pallet[map_value(value, min, max, range.min, range.max)];
}

// If 'spectrum' is in the range 0-159 it is converted to a spectrum colour
// from 0 = red through to 127 = blue to 159 = violet
// Extending the range to 0-191 adds a further violet to red band

static uint16_t rainbow_color_spectrum(uint8_t spectrum) {
  //uint8_t spectrum = map_value(value, min, max, uint8_t(0), uint8_t(191));
  spectrum = spectrum % 192;

  uint8_t red = 0; // Red is the top 5 bits of a 16 bit colour spectrum
  uint8_t green = 0; // Green is the middle 6 bits, but only top 5 bits used here
  uint8_t blue = 0; // Blue is the bottom 5 bits

  uint8_t sector = spectrum >> 5;
  uint8_t amplit = spectrum & 0x1F;

  switch (sector) {
  case 0:
    red = 0x1F;
    green = amplit; // Green ramps up
    blue = 0;
    break;
  case 1:
    red = 0x1F - amplit; // Red ramps down
    green = 0x1F;
    blue = 0;
    break;
  case 2:
    red = 0;
    green = 0x1F;
    blue = amplit; // Blue ramps up
    break;
  case 3:
    red = 0;
    green = 0x1F - amplit; // Green ramps down
    blue = 0x1F;
    break;
  case 4:
    red = amplit; // Red ramps up
    green = 0;
    blue = 0x1F;
    break;
  case 5:
    red = 0x1F;
    green = 0;
    blue = 0x1F - amplit; // Blue ramps down
    break;
  }

  return red << 11 | green << 6 | blue;
}

template<uint16_t W, uint16_t H>
class MyFB : public GFXcanvas16 {
public:
  static constexpr uint16_t fb_width = W;
  static constexpr uint16_t fb_height = H;
  heap_image_t<W, H> screen_framebuffer = {};
  MyFB();
};

template<uint16_t W, uint16_t H>
MyFB<W, H>::MyFB() : GFXcanvas16(W, H) {
  free(this->buffer);
  buffer = screen_framebuffer.pixels.data();
};
