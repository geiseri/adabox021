#pragma once

#include <cmath>
#include <vector>
#include <array>
#ifdef ESP32
#include <sdkconfig.h>
#else 
#define CONFIG_ESP_MAIN_TASK_STACK_SIZE 30616*4+1
#endif
template <size_t Start, size_t End, size_t Inc, class F>
constexpr void constexpr_for(F&& f) {
    if constexpr (Start < End) {
        f(std::integral_constant<decltype(Start), Start>());
        constexpr_for<Start + Inc, End, Inc>(f);
    }
}


template <typename I>
uint16_t get_pixel(const I& img, int16_t x, int16_t y) { return img.pixels[y * I::h + x]; }
template <typename I>
void set_pixel(const I& img, int16_t x, int16_t y, uint16_t color) { img.pixels[y * I::h + x] = color; }

inline uint16_t to_rgb565(int r, int g, int b) {
    return (uint16_t) ((r << 11) | (g << 5) | b);
}

namespace detail {
    inline uint16_t get_red(uint16_t col) { return (col >> 11) & 0xFF; }

    inline uint16_t get_green(uint16_t col) {
        return ((col >> 5) & ((1u << 6) - 1)) & 0xFF;
    }

    inline uint16_t get_blue(uint16_t col) { return (col & ((1u << 5) - 1)) & 0xFF; }
}

inline std::tuple<uint8_t, uint8_t, uint8_t> to_rgb32(uint16_t col) {
    return std::make_tuple(
        detail::get_red(col),
        detail::get_green(col),
        detail::get_blue(col)
    );
}



template<typename T>
void scroll_image(T& img, int16_t dx, int16_t dy) {
    // Fetch the scroll area width and height set by setScrollRect()
    uint32_t w = T::w - abs(dx); // line width to copy
    uint32_t h = T::h - abs(dy); // lines to copy
    int32_t iw = T::w; // rounded up width of sprite

    // Fetch the x,y origin set by setScrollRect()
    uint32_t tx = img.x; // to x
    uint32_t fx = img.x; // from x
    uint32_t ty = img.y; // to y
    uint32_t fy = img.y; // from y

    // Adjust for x delta
    if (dx <= 0) fx -= dx;
    else tx += dx;

    // Adjust for y delta
    if (dy <= 0) fy -= dy;
    else { // Scrolling down so start copy from bottom
        ty = ty + _sh - 1; // "To" pointer
        iw = -iw;          // Pointer moves backwards
        fy = ty - dy;      // "From" pointer
    }

    // Calculate "from y" and "to y" pointers in RAM
    uint32_t fyp = fx + fy * iw;
    uint32_t typ = tx + ty * iw;
    // if ty > fy for(y = h; y >= h-dy; y--)
    // if ty < fy for(y=0; y+dw < h; y++)
    // if tx >= fx std::move(tx,tx-w,fx)
    // if tx < fx std::move_backwards(tx,tx-w,fx-w)
    // Now move the pixels in RAM
    while (h--) { // move pixel lines (to, from, byte count)
        //memmove( _img + typ, _img + fyp, w<<1);
        std::copy_n(std::next(img.pixels.begin(), typ), w << 1, std::next(img.pixels.begin(), fyp));
        typ += iw;
        fyp += iw;
    }

    // Fill the gap left by the scrolling
    //if (dx > 0) fillRect(_sx, _sy, dx, _sh, _scolor);
    //if (dx < 0) fillRect(_sx + _sw + dx, _sy, -dx, _sh, _scolor);
    //if (dy > 0) fillRect(_sx, _sy, _sw, dy, _scolor);
    //if (dy < 0) fillRect(_sx, _sy + _sh + dy, _sw, -dy, _scolor);
}



template <int16_t W, int16_t H>
struct stack_image_t {
    static_assert((W* H) < (CONFIG_ESP_MAIN_TASK_STACK_SIZE / 4), "Allocation to large for stack");
    static constexpr int16_t  w = W;
    static constexpr int16_t  h = H;
    std::array<uint16_t, W* H> pixels = { {0} };
};

template <int16_t W, int16_t H>
struct raw_image_t {
    static constexpr int16_t w = W;
    static constexpr int16_t h = H;
    uint16_t* pixels = nullptr;
};

template <int16_t W, int16_t H, class Allocator >
struct heap_image_interface {
    static constexpr int16_t w = W;
    static constexpr int16_t h = H;
    std::vector<uint16_t, Allocator> pixels = std::vector<uint16_t, Allocator>(W* H);
};

template  <int16_t W, int16_t H>
using heap_image_t = heap_image_interface<W, H, std::allocator<uint16_t>>;

#ifdef ESP32
namespace detail {
    template <class T>
    struct PSallocator {
        typedef T value_type;
        PSallocator() = default;
        template <class U> constexpr PSallocator(const PSallocator<U>&) noexcept {}
        [[nodiscard]] T* allocate(std::size_t n) {
            if (n > std::size_t(-1) / sizeof(T)) throw std::bad_alloc();
            if (auto p = static_cast<T*>(ps_malloc(n * sizeof(T)))) return p;
            throw std::bad_alloc();
        }
        void deallocate(T* p, std::size_t) noexcept { std::free(p); }
    };
    template <class T, class U>
    bool operator==(const PSallocator<T>&, const PSallocator<U>&) { return true; }
    template <class T, class U>
    bool operator!=(const PSallocator<T>&, const PSallocator<U>&) { return false; }

} // namespace detail

template  <int16_t W, int16_t H>
using psram_image_t = heap_image_interface<W, H, detail::PSallocator<uint16_t>>;
#endif

