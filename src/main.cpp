// SPDX-FileCopyrightText: 2023 Limor Fried for Adafruit Industries
//
// SPDX-License-Identifier: MIT
//
// Original Example: https://github.com/adafruit/Adafruit_PyCamera/blob/main/examples/pycamera_test/pycamera_test.ino
//
#include <Arduino.h>
#include <anyrtttl.h>
#include <vector>
#include <memory>
#include <cstdint>
#include "context.h"
#include "helpers.h"
#include "rtos.h"
#include "microphone.h"
/*
ESP32-S3 module with 4 MB Flash, 2 MB PSRAM
*/



void setup() {
  auto main_task = new FreeRTOS::task_wrapper("main", ARDUINO_LOOP_STACK_SIZE, 1);
  auto mic_task = new FreeRTOS::task_wrapper("mic", ARDUINO_LOOP_STACK_SIZE, 2);
  auto buffer = new mic_stream_buffer();

  Serial.begin(115200);
  for (int idx = 0; idx < 5; idx++) {
    if (Serial.availableForWrite()) {
      break;
    }
    delay(10);
  }
  Serial.println("Here we go!!");


  Serial.setDebugOutput(true);
  multi_heap_info_t info;
  heap_caps_get_info(&info, MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT); // internal RAM, memory capable to store data or to create new task
  info.total_free_bytes;   // total currently free in all non-continues blocks
  info.minimum_free_bytes;  // minimum free ever
  info.largest_free_block;   // largest continues block to allocate big array
  Serial.printf("start: %d, min free: %d, largest free: %d\n", info.total_free_bytes, info.minimum_free_bytes, info.largest_free_block);

  if (!Context::instance()->setup_camera()) {
    while (true) {
      Serial.println("Failed to initialize pyCamera interface");
      delay(5000);
    }
  }
  heap_caps_get_info(&info, MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT); // internal RAM, memory capable to store data or to create new task
  info.total_free_bytes;   // total currently free in all non-continues blocks
  info.minimum_free_bytes;  // minimum free ever
  info.largest_free_block;   // largest continues block to allocate big array
  Serial.printf("camera up: %d, min free: %d, largest free: %d\n", info.total_free_bytes, info.minimum_free_bytes, info.largest_free_block);
#ifdef ENABLE_THERMAL_CAMERA
  if (!Context::instance()->setup_thermal_camera()) {
    while (true) {
      Serial.println("Failed to initialize thermal camera interface");
      delay(5000);
    }
    Serial.flush();
    //delay(5000);
  }
#endif
  Context::instance()->setup_tof_camera();
  Context::instance()->test_screen();
  anyrtttl::setToneFunction([](uint8_t pin, unsigned int frequency, unsigned long duration) {
    Context::instance()->cameraTone(pin, frequency, duration);
    });

  auto mic = new Microphone(buffer,40000);
  if (!mic_task->start([mic](FreeRTOS::task_wrapper *task) {
    mic->loop(task);
    // Sample at 8000 samples/s
/*     std::vector<mic_stream_buffer::value_type> sounds(buffer->size);
    analogSetPinAttenuation(GPIO_NUM_2, adc_attenuation_t::ADC_11db);

    uint32_t sample_period_us = std::round(11000000 * (1.f / 5000.f));
    while (true) {
      uint64_t us = micros();
      for (size_t dat = 0; dat < buffer->size; dat++) {
        sounds[dat] = analogRead(GPIO_NUM_2);
      }
      buffer->send(sounds);
      task->yield(); // give someone else a whack
    }
 */
    },1)) {
    while (true) {
      Serial.println("Failed to start mic task");
      delay(5000);
    }
  }

  if (main_task->start([buffer, mic](FreeRTOS::task_wrapper *task) {
    Context::instance()->mic = mic;
    while (true) {
      Profiler::instance()->outside_loop.end_stamp();
      Profiler::instance()->full_loop.start_stamp();
      //Serial.printf("Buttons: 0x%08X\n\r", pycamera.readButtons());
      //Context::instance()->test_screen();
      Context::instance()->loop_neopixel();
      Context::instance()->do_camera_grab();
#ifdef ENABLE_THERMAL_CAMERA
      Context::instance()->do_thermalcam_drawing();
#endif
      Context::instance()->update_tof_camera();
      Context::instance()->update_battery_stats();
      Context::instance()->update_microphone();
      Context::instance()->update_accelerometer_stats();
      Context::instance()->update_camera_stats();
      Context::instance()->send_to_screen();
      Context::instance()->handle_buttons();
      delay(1);
      Profiler::instance()->full_loop.end_stamp();
      Profiler::instance()->outside_loop.start_stamp();
    }
    },0)) {
    Profiler::instance()->outside_loop.start_stamp();
  }
  else {
    while (true) {
      Serial.println("Failed to start main loop");
      delay(5000);
    }
  }
}
void loop() {

}
