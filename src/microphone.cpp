#include "microphone.h"
#include <numeric>

Microphone::Microphone(mic_stream_buffer* s, mic_stream_buffer::value_type frequency) : stream(s), sample_frequency(frequency), sample_time(1000000UL / frequency) {
    buffer_ = std::vector<mic_stream_buffer::value_type>(stream->size);

    analogSetPinAttenuation(pin_, adc_attenuation_t::ADC_0db);

}


void Microphone::loop(FreeRTOS::task_wrapper* task) {
    while (true) {
        uint64_t start = micros();
        // 8k = 125uS      
        uint64_t nextTime = 0;
        for (size_t dat = 0; dat < stream->size; dat++) {
            while (micros() < nextTime) {
                task->yield();
            }
            // its inverted
            //buffer_[dat] = uint16_t(4095) - analogRead(pin_);
            // subtract 0.67 DC bias, take absolute value
            buffer_[dat] = std::max(analogReadMilliVolts(pin_) - 670.0f, 0.f);
            nextTime = micros() + sample_time;
        }

        stream->send(buffer_);
        task->yield(); // give someone else a whack
    }
}

void Microphone::update_fb(GFXcanvas16* dest) {
    //static constexpr uint16_t pixel_width = std::max(uint16_t(1), uint16_t(4.0 * fft_frame.h / stream->size));
    static constexpr uint16_t pixel_width = 1;

    auto buffer = stream->receive(200);
    // align to average


    //float mean = std::accumulate(buffer.begin(), buffer.end(), 0.f) / buffer.size();
    //std::transform(buffer.cbegin(), buffer.cend(), buffer.begin(), [mean](auto c) { return c - mean; });
    fft_offset_++;
    if (fft_offset_ >= fft_frame.w) fft_offset_ = 0;

    for (uint16_t index = 0; index < buffer.size(); index++) {
        uint16_t col = heat_color(buffer[index], 0.0f, 20.0f);
        fft_bmp->drawFastHLine(pixel_width * index,fft_offset_, pixel_width, col);
      }

   /* 
      std::vector<float> vimag(buffer.size());
      fft.windowing(buffer.data(), buffer.size(), FFT_WIN_TYP_HAMMING, FFT_FORWARD);
      fft.compute(buffer.data(), vimag.data(), buffer.size(), FFT_FORWARD);
      fft.complexToMagnitude(buffer.data(), vimag.data(), buffer.size());
      float maxFreq = 0.0;
      float maxMag = 0.0;
      fft.majorPeak(buffer.data(), buffer.size(), sample_frequency, &maxFreq, &maxMag);
      peak_mag.filter(maxMag);
      peak_frequeny.filter(maxFreq);


      for (uint16_t index = 0; index < buffer.size(); index++) {
        uint16_t col = heat_color(buffer[index], 0.0f, 280.0f);
        fft_bmp->drawFastHLine(pixel_width * index,fft_offset_, pixel_width, col);
      }
      */
      fft_bmp->drawFastHLine(0,fft_offset_+1,fft_frame.w,0);
    auto [min_e, max_e] = std::minmax_element(buffer.begin(), buffer.end());
    //sample_min.filter(*min_e);
    //sample_max.filter(*max_e);
    sample_min = *min_e;
    sample_max = *max_e;
    dest->drawRGBBitmap(fft_frame.x, fft_frame.y, fft_bmp->getBuffer(), fft_bmp->width(), fft_bmp->height());
}
