#pragma once
#include <Arduino.h>
#include <arduinoFFT.h>
#include <Ewma.h>

#include "helpers.h"
class Microphone {
    int pin_ = GPIO_NUM_17;
    std::vector<mic_stream_buffer::value_type> buffer_;
  uint16_t fft_offset_ = 0;

public:
    Microphone(mic_stream_buffer* s, mic_stream_buffer::value_type frequency = 40000.f);
    void loop(FreeRTOS::task_wrapper* task);
    mic_stream_buffer* stream = nullptr;
    const mic_stream_buffer::value_type sample_frequency;
    const mic_stream_buffer::value_type sample_time;
    ArduinoFFT<float> fft;
    //Ewma sample_min{0.1};
    //Ewma sample_max{0.1};
     float sample_min = 0;
    float sample_max = 0;
    Ewma peak_mag{0.1};
    Ewma peak_frequeny{0.1};
    static constexpr rect_t<0,240-60,60,60> fft_frame{};
    MyFB<fft_frame.w,fft_frame.h>* fft_bmp = new MyFB<fft_frame.w,fft_frame.h>();
    void update_fb(GFXcanvas16 *dest);
};