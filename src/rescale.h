#pragma once

#include <cmath>
#include <vector>

struct image_t
{
    uint16_t *pixels;
    int16_t w;
    int16_t h;
};
namespace foo {
inline uint16_t to_red(uint16_t col) { return (col >> 11) & 0xFF; }

inline uint16_t to_green(uint16_t col)
{
    return ((col >> 5) & ((1u << 6) - 1)) & 0xFF;
}

inline uint16_t to_blue(uint16_t col) { return (col & ((1u << 5) - 1)) & 0xFF; }

inline uint16_t to_rgb565(int r, int g, int b)
{
    return (uint16_t)((r << 11) | (g << 5) | b);
}

uint16_t getpixel(image_t *image, int16_t x, int16_t y)
{
    return image->pixels[(y * image->w) + x];
}

void putpixel(image_t *image, int16_t x, int16_t y, uint16_t color)
{
    image->pixels[(y * image->w) + x] = color;
}

int blend_pixel(image_t *original_img, int16_t x, int16_t y, int16_t x_floor, int16_t x_ceil, int16_t y_floor, int16_t y_ceil, uint16_t (*color_call)(uint16_t))
{

    if (x_ceil == x_floor && y_ceil == y_floor)
    {
        return (*color_call)(getpixel(original_img, x, y));
    }
    else if (x_ceil == x_floor)
    {
        auto q1 = (*color_call)(getpixel(original_img, x, y_floor));
        auto q2 = (*color_call)(getpixel(original_img, x, y_ceil));
        return q1 * (y_ceil - y) + q2 * (y - y_floor);
    }
    else if (y_ceil == y_floor)
    {
        auto q1 = (*color_call)(getpixel(original_img, x_floor, y));
        auto q2 = (*color_call)(getpixel(original_img, x_ceil, y));
        return (q1 * (x_ceil - x)) + (q2 * (x - x_floor));
    }
    else
    {
        auto v1 = (*color_call)(getpixel(original_img, x_floor, y_floor));
        auto v2 = (*color_call)(getpixel(original_img, x_ceil, y_floor));
        auto v3 = (*color_call)(getpixel(original_img, x_floor, y_ceil));
        auto v4 = (*color_call)(getpixel(original_img, x_ceil, y_ceil));
        auto q1 = v1 * (x_ceil - x) + v2 * (x - x_floor);
        auto q2 = v3 * (x_ceil - x) + v4 * (x - x_floor);
        return q1 * (y_ceil - y) + q2 * (y - y_floor);
    }
}
}
void bl_resize(image_t *original_img, image_t *new_img)
{
    // Calculate horizontal and vertical scaling factor
    auto w_scale_factor = new_img->h != 0 ? ((float)original_img->w) / (new_img->w) : 0;
    auto h_scale_factor = new_img->w != 0 ? ((float)original_img->h) / (new_img->h) : 0;

    for (int16_t i = 0; i < new_img->w; i++)
    {
        auto x = i * w_scale_factor;
        auto x_floor = std::floor(x);
        auto x_ceil = std::min(original_img->w - 1.0f, std::ceil(x));
        for (int16_t j = 0; j < new_img->h; j++)
        {
            auto y = j * h_scale_factor;
            int16_t y_floor = std::floor(y);
            int16_t y_ceil = std::min(original_img->h - 1.0f, std::ceil(y));
            auto col = foo::to_rgb565(
                foo::blend_pixel(original_img, x, y, x_floor, x_ceil, y_floor, y_ceil, &foo::to_red),
                foo::blend_pixel(original_img, x, y, x_floor, x_ceil, y_floor, y_ceil, &foo::to_green),
                foo::blend_pixel(original_img, x, y, x_floor, x_ceil, y_floor, y_ceil, &foo::to_blue));
            foo::putpixel(new_img, i, j, col);
        }
    }
}
