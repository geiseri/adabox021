#pragma once

#include "image.h"

namespace bar {

    inline uint16_t to_red(uint16_t col) { return (col >> 11) & 0xFF; }

    inline uint16_t to_green(uint16_t col) {
        return ((col >> 5) & ((1u << 6) - 1)) & 0xFF;
    }

    inline uint16_t to_blue(uint16_t col) { return (col & ((1u << 5) - 1)) & 0xFF; }

    inline uint16_t to_rgb565(int r, int g, int b) {
        return (uint16_t) ((r << 11) | (g << 5) | b);
    }

    template <typename ImgImpl>
    uint16_t getpixel(ImgImpl* image, int16_t x, int16_t y) {
        //
        auto px = image->pixels[(y * ImgImpl::w) + x];
        return (px >> 8) | (px << 8);
    }

    template <typename ImgImpl>
    void putpixel(ImgImpl* image, int16_t x, int16_t y, uint16_t color) {
        image->pixels[(y * ImgImpl::w) + x] = color;
    }

    template <typename ImgImpl, typename Cf>
    int blend_pixel(ImgImpl* original_img, int16_t x, int16_t y, int16_t x_floor, int16_t x_ceil, int16_t y_floor, int16_t y_ceil, Cf color_call) {

        if (x_ceil == x_floor && y_ceil == y_floor) {
            return (*color_call)(getpixel(original_img, x, y));
        }
        else if (x_ceil == x_floor) {
            auto q1 = color_call(getpixel(original_img, x, y_floor));
            auto q2 = color_call(getpixel(original_img, x, y_ceil));
            return q1 * (y_ceil - y) + q2 * (y - y_floor);
        }
        else if (y_ceil == y_floor) {
            auto q1 = color_call(getpixel(original_img, x_floor, y));
            auto q2 = color_call(getpixel(original_img, x_ceil, y));
            return (q1 * (x_ceil - x)) + (q2 * (x - x_floor));
        }
        else {
            auto v1 = color_call(getpixel(original_img, x_floor, y_floor));
            auto v2 = color_call(getpixel(original_img, x_ceil, y_floor));
            auto v3 = color_call(getpixel(original_img, x_floor, y_ceil));
            auto v4 = color_call(getpixel(original_img, x_ceil, y_ceil));
            auto q1 = v1 * (x_ceil - x) + v2 * (x - x_floor);
            auto q2 = v3 * (x_ceil - x) + v4 * (x - x_floor);
            return q1 * (y_ceil - y) + q2 * (y - y_floor);
        }
    }
}
template <typename ImgImpl1, typename ImgImpl2>
void const_bl_resize(ImgImpl1* original_img, ImgImpl2* new_img) {
    // Calculate horizontal and vertical scaling factor
    constexpr auto w_scale_factor = (float) ImgImpl1::w / ImgImpl2::w;
    constexpr auto h_scale_factor = (float) ImgImpl1::h / ImgImpl2::h;

    //constexpr_for<0, ImgImpl2::w, 1>([original_img,new_img](auto i){
    for (int16_t i = 0; i < ImgImpl2::w; i++) {
        auto x = i * w_scale_factor;
        auto x_floor = std::floor(x);
        auto x_ceil = std::min(ImgImpl1::w - 1.0f, std::ceil(x));
        //constexpr_for<0, ImgImpl2::h, 1>([original_img,new_img,i,x,x_floor,x_ceil](auto j){
        for (int16_t j = 0; j < ImgImpl2::h; j++) {
            auto y = j * h_scale_factor;
            int16_t y_floor = std::floor(y);
            int16_t y_ceil = std::min(ImgImpl1::h - 1.0f, std::ceil(y));
            auto col = bar::to_rgb565(
                bar::blend_pixel(original_img, x, y, x_floor, x_ceil, y_floor, y_ceil, &bar::to_red),
                bar::blend_pixel(original_img, x, y, x_floor, x_ceil, y_floor, y_ceil, &bar::to_green),
                bar::blend_pixel(original_img, x, y, x_floor, x_ceil, y_floor, y_ceil, &bar::to_blue));
            bar::putpixel(new_img, i, j, col);
            //});
        }
        //});
    }
}
