#include "rtos.h"

namespace FreeRTOS {
    task_wrapper::lock_guard::lock_guard(task_wrapper* parent) : parent_(parent) {
        parent_->lock(10);

    }

    task_wrapper::lock_guard::~lock_guard() {
        parent_->unlock();
    }


    task_wrapper::task_wrapper(const std::string& Name, uint16_t StackDepth, UBaseType_t Priority) : name_(Name), stack_depth_(StackDepth), priority_(Priority), delay_until_initialized_(false) {

    }

    task_wrapper::~task_wrapper() {

    }

    bool task_wrapper::start(std::function<void(task_wrapper*)> task) {
        task_ = task;
        return xTaskCreate(task_adaptor, name_.c_str(), stack_depth_, this, priority_, &handle_) == pdPASS;
    }

    bool task_wrapper::start(std::function<void(task_wrapper*)> task, const BaseType_t xCoreID) {
        task_ = task;
        return xTaskCreatePinnedToCore(task_adaptor, name_.c_str(), stack_depth_, this, priority_, &handle_, xCoreID) == pdPASS;
    }

    TaskHandle_t task_wrapper::handle() {
        return handle_;
    }

    void task_wrapper::yield() {
        taskYIELD();
    }
    void task_wrapper::suspend() {
        vTaskSuspend(handle_);
    }
    void task_wrapper::resume() {
        vTaskResume(handle_);
    }
    void task_wrapper::resume_from_isr() {
        xTaskResumeFromISR(handle_);
    }
    UBaseType_t task_wrapper::get_priority() {
        return (uxTaskPriorityGet(handle_));
    }
    UBaseType_t task_wrapper::get_priority_from_isr() {
        return (uxTaskPriorityGetFromISR(handle_));
    }
    void task_wrapper::set_priority(UBaseType_t NewPriority) {
        vTaskPrioritySet(handle_, NewPriority);
    }
    std::string task_wrapper::get_name() {
        return name_;
    }
    bool task_wrapper::lock(TickType_t Timeout) {
        return pdTRUE == xSemaphoreTake(mutex_, Timeout);
    }

    bool task_wrapper::unlock() {
        return pdTRUE == xSemaphoreGive(mutex_);
    }

    bool task_wrapper::is_running() {
        return eTaskGetState(handle_) == eRunning;
    }

    void task_wrapper::delay(const TickType_t Delay) {
        vTaskDelay(Delay);
    }
    void task_wrapper::delay_until(const TickType_t Period) {
        if (!delay_until_initialized_) {
            delay_until_initialized_ = true;
            delay_until_previous_wake_time_ = xTaskGetTickCount();
        }

        vTaskDelayUntil(&delay_until_previous_wake_time_, Period);
    }

    void task_wrapper::reset_delay_until() {
        delay_until_initialized_ = false;
    }
    void task_wrapper::task_adaptor(void* pvParameters) {
        task_wrapper* tsk = static_cast<task_wrapper*>(pvParameters);
        tsk->mutex_ = xSemaphoreCreateMutex();
        tsk->task_(tsk);
        vSemaphoreDelete(tsk->mutex_);
        vTaskDelete(tsk->handle_);
    }

} // namespace FreeRTOS
