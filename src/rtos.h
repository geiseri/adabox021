#pragma once

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <stream_buffer.h>
#include <string>
#include <functional>

namespace FreeRTOS {

    class task_wrapper {

    public:

        class lock_guard {
        public:
            lock_guard(task_wrapper* parent);
            ~lock_guard();
        private:
            task_wrapper* parent_ = nullptr;
        };

        task_wrapper(const std::string& Name, uint16_t StackDepth, UBaseType_t Priority);
        ~task_wrapper();

        TaskHandle_t handle();

        bool start(std::function<void(task_wrapper*)> task);
        bool start(std::function<void(task_wrapper*)> task, const BaseType_t xCoreID);
        static  void yield();

        void suspend();
        void resume();
        void resume_from_isr();
        UBaseType_t get_priority();
        UBaseType_t get_priority_from_isr();
        void set_priority(UBaseType_t NewPriority);
        std::string get_name();
        bool lock(TickType_t Timeout);
        bool unlock();
        bool is_running();

        void delay(const TickType_t Delay);
        void delay_until(const TickType_t Period);
        void reset_delay_until();

    private:
        static void task_adaptor(void* pvParameters);
        std::function<void(task_wrapper*)> task_;
        TaskHandle_t handle_;
        SemaphoreHandle_t mutex_;

        const std::string name_;
        const uint16_t stack_depth_;
        UBaseType_t priority_;
        bool delay_until_initialized_;
        TickType_t delay_until_previous_wake_time_;
    };

    template<size_t N, typename T = uint8_t>
    class stream_buffer {
    public:
        static constexpr size_t size = N;
        static constexpr size_t n_size =  sizeof(T)/sizeof(uint8_t);
        using value_type = T;
        stream_buffer(size_t trigger = N-1) {
            handle_ = xStreamBufferCreate(N * n_size, trigger);
        }
        ~stream_buffer() {
            vStreamBufferDelete(handle_);
        }

        size_t send(const std::vector<T>& data, int block_for = 0) {
            int ticks = block_for > 0 ? pdMS_TO_TICKS(block_for) : 1; 
            return xStreamBufferSend(handle_, data.data(), data.size() * n_size, ticks);
        }

        std::vector<T> receive(int block_for = 0) {
            int ticks = block_for > 0 ? pdMS_TO_TICKS(block_for) : 1; 
            std::vector<T> buffer(N);
            auto read = xStreamBufferReceive(handle_, buffer.data(), N * n_size, ticks);
            buffer.resize(read/n_size);
            return buffer;
        }

        size_t buffer_space_available() {
            return xStreamBufferSpacesAvailable(handle_);
        }
        size_t buffer_bytes_available() {
            return xStreamBufferBytesAvailable(handle_);
        }
        bool is_empty() {
            return xStreamBufferIsEmpty(handle_) == pdTRUE;
        }

        bool is_full() {
            return xStreamBufferIsFull(handle_) == pdTRUE;
        }

        bool reset() {
            return xStreamBufferReset(handle_) == pdTRUE;
        }

    private:
        StreamBufferHandle_t handle_;
    };
}
