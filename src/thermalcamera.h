#pragma once
#include <array>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <Adafruit_MLX90640.h>
#include <cstdint>
#include <Ewma.h>
//#include <PNGenc.h>
#include "helpers.h"
#include <Arduino.h>
#include <Adafruit_GFX.h> 


struct temp_range {
  int min;
  int max;
  std::string name;
};


template <size_t R = 24, size_t C = 32>
struct ThermalCamera {
  static constexpr size_t rows = R;
  static constexpr size_t cols = C;
  int grab_delta = 0;
  int calc_delta = 0;
  std::array<float, R* C> frame_buffer = { {0} };
  std::string display_string;
  size_t current_range = 5;
  Ewma filterMin{ 0.05 };
  Ewma filterMax{ 0.1 };
  std::vector<temp_range> ranges = {
      temp_range{10, 32, "Wide"},
      temp_range{5, 32, "Mammals"},
      temp_range{20, 32, "Humans"},
      temp_range{20, 50, "Hot"},
      temp_range{-10, 200, "Extreme"},
      temp_range{0, 255, "Auto"},
  };

  Adafruit_MLX90640 mlx;

  bool begin() {
    filterMin.filter(-50);
    filterMax.filter(50);
    if (mlx.begin(MLX90640_I2CADDR_DEFAULT, &Wire)) {
      mlx.setMode(MLX90640_CHESS);
      mlx.setResolution(MLX90640_ADC_18BIT);
      mlx.setRefreshRate(MLX90640_32_HZ);
      Wire.setClock(1000000); // max 1 MHz
      return true;
    }
    return false;
  }

  void range_next() {
    set_range(current_range + 1);
  }
  void range_prev() {
    set_range(current_range - 1);
  }

  void set_range(size_t idx) {

    current_range = std::clamp(idx, 0u, ranges.size() - 1);
  }

  void set_fast_clock() {
    mlx.setRefreshRate(MLX90640_64_HZ);
  }

  void set_slow_clock() {
    mlx.setRefreshRate(MLX90640_16_HZ);
  }

  bool capture() { return mlx.updatePartialFrame(frame_buffer.data()); }
  float get(int x, int y) { return frame_buffer[(y * cols) + x]; }
  std::pair<float,float> get_minmax()
  {
    auto [min,max] = std::minmax_element(frame_buffer.begin(), frame_buffer.end());
    return std::make_pair(*min,*max);
  }

  template<uint16_t X_POS, uint16_t Y_POS, uint16_t BOX_WIDTH, uint16_t BOX_HEIGHT>
  void draw(GFXcanvas16* fb) {
    static constexpr int line_width = 5;
    constexpr float displayPixelWidth = std::ceil(float(BOX_WIDTH) / cols);
    constexpr float displayPixelHeight = std::ceil(float(BOX_HEIGHT - line_width) / rows);

    grab_delta = mlx.grab_end_time - mlx.grab_start_time;
    calc_delta = mlx.calculate_end_time - mlx.calculate_start_time;

    color_range cr{ 0, 0 };
    temp_range& range = ranges[current_range % ranges.size()];
    float mintemp = range.min;
    float maxtemp = range.max;
    std::string title = range.name;
    if (title == "Auto") {
      auto delta = (maxtemp - mintemp) * 0.1f;
      mintemp -= delta;
      maxtemp += delta;
    }
    auto [currmin,currmax] = get_minmax();
    for (uint8_t h = 0; h < rows; h++) {
      for (uint8_t w = 0; w < cols; w++) {
        float pixel = get(w, rows - h - 1);
        // draw the pixels!
        fb->fillRect(
          X_POS + (displayPixelWidth * w),
          Y_POS + (displayPixelHeight * h),
          displayPixelWidth, displayPixelHeight,
          heat_color<float>(pixel, mintemp, maxtemp)
        );
      }
    }

    ranges.back().min = filterMin.filter(currmin) - 1;
    ranges.back().max = filterMax.filter(currmax) + 1;
    cr.max = map_value(currmax, mintemp, maxtemp, 0, 255);
    cr.min = map_value(currmin, mintemp, maxtemp, 0, 255);

    for (uint16_t x = 0; x < BOX_WIDTH; x++) {
      fb->drawLine(X_POS + x, Y_POS + BOX_HEIGHT, X_POS + x, Y_POS + BOX_HEIGHT - line_width, heat_color<float>(x, 0, BOX_WIDTH, cr));
    }

    if (title == "Auto") {
      display_string = "Range:";
      display_string.append(std::to_string(ranges.back().min));
      display_string.append("C ");
      display_string.append(std::to_string(ranges.back().max));
      display_string.append("C");
    }
    else {
      display_string = title;
    }
  }



  std::string get_report() {
    std::stringstream stream;

    stream << std::setprecision(4) << "\"\"";
    for (uint8_t w = 0; w < cols; w++) {
      stream << "," << w + 1;
    }
    stream << std::endl;

    for (uint8_t h = 0; h < rows; h++) {
      stream << h + 1;
      for (uint8_t w = 0; w < cols; w++) {
        stream << "," << get(w, rows - h - 1);
      }
      stream << std::endl;
    }
    return stream.str();
  }
};
