#include "tofcamera.h"

void TofCamera::setup() {
    if (resolution == resolutions::res_4x4)
{    tof_camera = new VL53L5CX_Arduino(14, 5, VL53L5CX::RES_4X4_HZ_15);
}else {
    tof_camera = new VL53L5CX_Arduino(14, 5, VL53L5CX::res8X8_t::RES_8X8_HZ_15);
}    tof_camera->begin();
}

std::pair<uint16_t, uint16_t> TofCamera::get_minmax() {
    auto [min, max] = std::minmax_element(frame_buffer.begin(), frame_buffer.end());
    return std::make_pair(*min, *max);
}
uint16_t TofCamera::get_pixel(int x, int y) {
    return frame_buffer[(y * cols) + x];
}
void  TofCamera::update() {
    if (tof_camera->dataIsReady()) {
        tof_camera->readData();
        frame_buffer.resize(tof_camera->getPixelCount());
        for (auto i = 0; i < tof_camera->getPixelCount(); i++) {
            frame_buffer[i] = tof_camera->getDistanceMm(i);
        }

    }
}


  void TofCamera::range_next() {
    set_range(current_range + 1);
  }
  void TofCamera::range_prev() {
    set_range(current_range - 1);
  }

  void TofCamera::set_range(size_t idx) {

    current_range = std::clamp(idx, 0u, ranges.size() - 1);
  }
