#pragma once
#include <array>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <EwmaT.h>
#include "helpers.h"
#include <Arduino.h>
#include <Adafruit_GFX.h> 
#include "vl53l5cx_arduino.h"

struct distance_range {
    uint16_t min;
    uint16_t max;
    std::string name;
};

struct TofCamera {
    enum class resolutions {
        res_4x4,
        res_8x8
    };
    static constexpr resolutions resolution = resolutions::res_4x4;
    static constexpr size_t rows = resolution == resolutions::res_4x4 ? 4 : 8;
    static constexpr size_t cols = resolution == resolutions::res_4x4 ? 4 : 8;
    int grab_delta = 0;
    int calc_delta = 0;
    VL53L5CX_Arduino* tof_camera = nullptr;
    std::vector<uint16_t> frame_buffer;
    std::string display_string;
    EwmaT<uint16_t> filterMin{ 7, 10 };
    EwmaT<uint16_t> filterMax{ 7, 10};
    size_t current_range = 1;

    std::vector<distance_range> ranges = {
        distance_range{0, 100, "100mm"},
        distance_range{0, 400, "400mm"},
        distance_range{0, 800, "800mm"},
        distance_range{0, 1000, "1000m"},
        distance_range{0, 2000, "2000m"},
        distance_range{0, 0, "Auto"},
    };


  void range_next() ;
  void range_prev() ;
  void set_range(size_t idx) ;


    uint16_t get_pixel(int x, int y);
    std::pair<uint16_t, uint16_t> get_minmax();
    void setup();
    template<uint16_t X_POS, uint16_t Y_POS, uint16_t BOX_WIDTH, uint16_t BOX_HEIGHT>
    void draw(GFXcanvas16* fb) {
          static constexpr int line_width = 5;
    constexpr float displayPixelWidth = std::ceil(float(BOX_WIDTH) / cols);
    constexpr float displayPixelHeight = std::ceil(float(BOX_HEIGHT - line_width) / rows);

    color_range cr{ 0, 0 };
    distance_range& range = ranges[current_range % ranges.size()];
    auto mintemp = range.min;
    auto maxtemp = range.max;
    if (range.name == "Auto") {
      auto delta = (maxtemp - mintemp) * 0.1f;
      mintemp -= delta;
      maxtemp += delta;
    }
    auto [currmin,currmax] = get_minmax();
    for (uint8_t h = 0; h < rows; h++) {
      for (uint8_t w = 0; w < cols; w++) {
        float pixel = get_pixel(w, rows - h - 1);
        // draw the pixels!
        fb->fillRect(
          X_POS + (displayPixelWidth * w),
          Y_POS + (displayPixelHeight * h),
          displayPixelWidth, displayPixelHeight,
          heat_color<float>(pixel, mintemp, maxtemp)
        );
      }
    }

    ranges.back().min = filterMin.filter(currmin) - 1;
    ranges.back().max = filterMax.filter(currmax) + 1;
    cr.max = map_value(currmax, mintemp, maxtemp, 0, 255);
    cr.min = map_value(currmin, mintemp, maxtemp, 0, 255);

    for (uint16_t x = 0; x < BOX_WIDTH; x++) {
      fb->drawLine(X_POS + x, Y_POS + BOX_HEIGHT, X_POS + x, Y_POS + BOX_HEIGHT - line_width, heat_color<float>(x, 0, BOX_WIDTH, cr));
    }
        display_string = "Range:";
        display_string.append(std::to_string(currmin));
        display_string.append("mm ");
        display_string.append(std::to_string(currmax));
        display_string.append("mm ");
        display_string.append(std::to_string(cr.min));
        display_string.append(" ");
        display_string.append(std::to_string(cr.max));
        display_string.append(" ");
    }
    void update();
};