// g++ lodepng.cpp main.cpp -ansi -std=c++17 -Wall -Wextra -O3 -o scale -I ../src
#include "lodepng.h"
#include <iostream>
#include <tuple>
#include "rescale.h"
#include "rescale_const.h"


std::vector<uint16_t> rgba8888torgb565(std::vector<uint8_t> image)
{
    std::vector<uint16_t> out;
    for (std::size_t idx = 0; idx < image.size();)
    {
        uint8_t red = image[idx];
        idx++;
        uint8_t green = image[idx];
        idx++;
        uint8_t blue = image[idx];
        idx++;
        idx++;
        uint16_t b = (blue >> 3) & 0x1f;
        uint16_t g = ((green >> 2) & 0x3f) << 5;
        uint16_t r = ((red >> 3) & 0x1f) << 11;
        out.push_back(r | g | b);
    }
    return out;
}

std::vector<uint8_t> rgb565torgba8888(std::vector<uint16_t> image)
{
    std::vector<uint8_t> out;
    for (auto pix:image)
    {
        out.push_back((uint8_t)(((pix & 0xF800) >> 11) << 3));
        out.push_back((uint8_t)(((pix & 0x7E0) >> 5) << 2));
        out.push_back((uint8_t)(((pix & 0x1F)) << 3));
        out.push_back(255);
    }
    return out;
}

template <typename T>
void print_vector(std::vector<T> v)
{
    for (auto px : v)
    {
        std::cout << px << " ";
    }
    std::cout << std::endl;
}
// Example 1
// Decode from disk to raw pixels with a single function call
auto decodeOneStep(const char *filename)
{
    std::vector<unsigned char> image; // the raw pixels
    unsigned width, height;

    // decode
    unsigned error = lodepng::decode(image, width, height, filename);

    // if there's an error, display it
    if (error)
        std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

    // the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
    return std::make_tuple(image, width, height);
}

void encodeOneStep(const char *filename, std::vector<unsigned char> &image, unsigned width, unsigned height)
{
    // Encode the image
    unsigned error = lodepng::encode(filename, image, width, height);

    // if there's an error, display it
    if (error)
        std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;
}

int main(int argc, char *argv[])
{
    const char *filename = argc > 1 ? argv[1] : "test.png";
    auto [image, width, height] = decodeOneStep(filename);
    auto inimg = rgba8888torgb565(image);
    std::cout << "resize from " << width << " height " << height << std::endl;

#ifdef XXX
    const_image_t<172,178> large{};
    std::copy(inimg.begin(),inimg.end(),large.pixels.begin());
    heap_image_t<120,200> small{};
    const_bl_resize(&large,&small);
    auto outimg = rgb565torgba8888(std::vector<uint16_t>(small.pixels.begin(),small.pixels.end()));
    encodeOneStep("out-const.png", outimg, small.w, small.h);
#else
    image_t large2{
        inimg.data(),
        width,
        height
    };
    image_t small2{
        pixels:nullptr,
        w:120,
        h:200
    };
    std::vector<uint16_t> outbuffer(small2.h*small2.w);
    small2.pixels = outbuffer.data();
    bl_resize(&large2,&small2);

    auto outimg2 = rgb565torgba8888(std::vector<uint16_t>(outbuffer));
    encodeOneStep("out-non.png", outimg2, small2.w, small2.h);
#endif
}